from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


# `ScrumyGoals` which will keep record of each goals
# `ScrumyHistory` which will store information about activities/actions carried out on a goal
# `GoalStatus` which will store all possible status of a goal.


class GoalStatus(models.Model):
    status_name = models.CharField(max_length= 30)

    def __str__(self):
        return self.status_name

class ScrumyGoals(models.Model):
    goal_name = models.CharField(max_length= 30)
    goal_id = models.IntegerField()
    created_by = models.CharField(max_length= 30)
    moved_by = models.CharField(max_length= 30)
    owner = models.CharField(max_length= 30)
    goal_status=models.ForeignKey(GoalStatus, on_delete = models.PROTECT)
    user = models.ForeignKey(User, related_name = 'goal_created_by', on_delete = models.PROTECT)

    def __str__(self):
        return self.goal_name

class ScrumyHistory(models.Model):
    moved_by = models.CharField(max_length= 30)
    created_by = models.CharField(max_length= 30)
    moved_from = models.CharField(max_length= 30)
    moved_to = models.CharField(max_length= 30)
    time_of_action = models.DateTimeField(default = timezone.now)
    goal = models.ForeignKey(ScrumyGoals, on_delete = models.PROTECT)

    def __str__(self):
        return self.created_by

