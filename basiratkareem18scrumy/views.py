from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.contrib.auth.models import User
from .models import ScrumyGoals, ScrumyHistory, GoalStatus
import random
# Create your views here.


def index(request):
    output = ScrumyGoals.objects.filter(goal_name = 'Learn Django')
    return HttpResponse(output)

def move_goal(request, goal_id):
    try:
         obj = ScrumyGoals.objects.get(goal_id=goal_id)
    except ScrumyGoals.DoesNotExist:
        raise Http404("A record with that goal id does not exist")
    return render(request, 'basiratkareem18scrumy/exception.html', {'obj': obj})

def add_goal(request):
    user = User.objects.get(username = 'Louis Oma')
    goal = GoalStatus.objects.create(status_name = 'Weekly Goal')
    random_id = random.randint(1000, 9999)
    goals = ScrumyGoals.objects.filter(goal_id = random_id).exists()
    if not goals:
        data = ScrumyGoals.objects.create(goal_name='Keep Learning Django', goal_id= random_id,
                    created_by='Louis', moved_by='Louis', owner='Louis',
                            goal_status=goal , user= user)
        data.save()
        return HttpResponse("New Goal Added")

def home(request):
    context  = {
        'goal_name': 'Learn Django',
        'goal_id': 1,
        'user': User.objects.get(first_name= 'louis')
    }
    return render(request, 'basiratkareem18scrumy/home.html', context)
